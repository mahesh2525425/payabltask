import requests
import pytest

# Base URL for the JSONPlaceholder API
BASE_URL = "https://jsonplaceholder.typicode.com/posts"

# Test data for creating and updating posts
TEST_POST_DATA = {
    "title": "Test Title",
    "body": "Test Body",
    "userId": 1
}

# Fixture to create a new post and return its ID
@pytest.fixture(scope="function")
def create_post():
    response = requests.post(BASE_URL, json=TEST_POST_DATA)
    assert response.status_code == 201
    assert response.json()["title"] == TEST_POST_DATA["title"]
    assert response.json()["body"] == TEST_POST_DATA["body"]
    assert response.json()["userId"] == TEST_POST_DATA["userId"]
    post_id = response.json()["userId"]
    return post_id

# Test scenario for reading a post
def test_read_post(create_post):
    post_id = create_post
    response = requests.get(f"{BASE_URL}/{post_id}")
    assert response.status_code == 200
    assert response.json()["id"] == post_id

# Test scenario for updating a post
def test_update_post(create_post):
    post_id = create_post
    updated_data = TEST_POST_DATA.copy()
    updated_data["title"] = "Updated Title"
    response = requests.put(f"{BASE_URL}/{post_id}", json=updated_data)
    assert response.status_code == 200
    assert response.json()["title"] == "Updated Title"

# Test scenario for deleting a post
def test_delete_post(create_post):
    post_id = create_post
    response = requests.delete(f"{BASE_URL}/{post_id}")
    assert response.status_code == 200
