# Steps to run the CRUD Operations test cases
1. Install visual studio from the offical site https://code.visualstudio.com/
2. Install phyton on your system
   Download Python:
   Visit the official Python website at Python.org.
   Navigate to the Downloads section and select the latest version of Python for Windows system
   Run the Installer:
   Once the download is complete, locate the downloaded file (it should be named something like python-3.x.x-amd64.exe, where 3.x.x is the version number).
   Double-click the installer to run it.
   Installation Options:
   In the installer window, make sure to check the box that says “Add Python 3.x to PATH” before you click “Install Now”. This will make it easier to run Python from the command line.
   Complete the Installation:
   Follow the prompts in the installer, keeping the default settings unless you have a specific need to change them.
   Once the installation is complete, you may need to restart your computer.
   Verify the Installation:
   Open the Command Prompt by typing cmd in the Windows search bar and hitting Enter.
   In the Command Prompt, type python --version and press Enter. This should display the version of Python that you just installed.
3. Open Visual studio and Add phyton extension
4. To run these tests, ensure you have pytest and requests installed in your Python environment. You can install them using pip
   install pip install pytest requests
5. clone the project into your Vscode from gitlab repo
6. Open the Vscode terminal and run test_CrudOperation.py test using following command
   Pytest test_CrudOperation.py